function [turnhist,counts,bins] = TurnHistPlot( lossesperel, Nbins )
        %% Number of truns performed by lost particles
        %

        % 30042019 unclear error on test
        test=arrayfun( @(el)isempty(el{:}),lossesperel);
        lossesperel(test)=[];
        lossesperel = cell2mat(lossesperel);
        %
        
        
        lossturns = cell2mat(arrayfun( @(el) el.turn, lossesperel, 'UniformOutput', false));
        lossturns(find(isnan(lossturns)+isinf(lossturns)))=[];
        
        turnhist = figure('Name','Turns','units','normalized','position',[0.35 0.4 0.5 0.5]);
        set(gcf,'Color',[1. 1. 1.]);
        
        turnmax = max( lossturns );
        [counts,  bins] = hist( lossturns, Nbins );
        counts = counts/sum(counts)*100;
%        semilogy(bins, counts, 'r *', 'MarkerSize', 10 )
        plot(bins, cumsum(counts), 'r *', 'MarkerSize', 10 )
        
%        axis([ 0, 1.1*turnmax, 0, 1.1*max(counts) ]);
        axis([ 0, 1.1*turnmax, 0, 110 ]);
        xlabel('Number of Turns (m)','FontSize',14);
        ylabel('Percentage of lost particles (%)','FontSize',14);
        title('Percentage of losses per number of turns performed (cumulative)')
        set(findall(findobj(gcf),'Type','axes'),'FontName','Arial','FontWeight','Bold','LineWidth',2,'FontSize',14);
        set(findall(findobj(gcf),'Type','text'),'FontName','Arial','FontWeight','Bold','FontSize',14);
        grid on; box on;
               
        
        
        
    end

