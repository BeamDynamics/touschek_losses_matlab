    function deltahist = DeltaHist2DPlot( Sstartindex, lossesperel, Nbins, flagtrack, varargin )
        %% Energy (when scattered/lost) distribution of scattererd particles.
        %        if flagtrack
        %    title('Energy distribution when lost')
        %else
        %    title('Energy distribution when scattered')
        %end

        
        if nargin>4
            deltavsthickelement = varargin{1};
        else
            deltavsthickelement =[];   
        end

        if ~flagtrack && length(lossesperel)~=length(Sstartindex)
             disp('Loss data do not match length of starting elements table.')
             return
        elseif ~flagtrack
             lossdeltas = arrayfun( @(el)  el.ddelta, lossesperel, 'UniformOutput', false);
        elseif flagtrack
             lossdeltas = arrayfun( @(el)  el.coordinates(5,:,end), lossesperel, 'UniformOutput', false);
        end

        deltahist = figure('Name','2D - Delta distribution','units','normalized','position',[0.35 0.4 0.5 0.5]); 
        set(gcf,'Color',[1. 1. 1.]);
        
%         fldeltas = cell2mat(lossdeltas);
%         fldeltas(isnan(fldeltas)|fldeltas==0) = []
%         deltamax = max(abs(fldeltas));
deltamax=0.3;
        for el=1:length(lossesperel)
            lossdeltas{el}(lossdeltas{el}==0)=[];
            [countsel{el}, bins] = hist( lossdeltas{el}, linspace(-1.1*deltamax, 1.1*deltamax, Nbins) );
        end
        counts = cell2mat(countsel');

        [sg, dg] = meshgrid(Sstartindex, bins);

        surf(sg, dg, counts', 'EdgeColor', 'none'); 
        %colormap('Pink');
        view(0,90);
        colorbar ;
        hold on;
        if ~isempty(deltavsthickelement)
        plot(Sstartindex, deltavsthickelement(:,1), 'black-',...
             Sstartindex, deltavsthickelement(:,2), 'black-','linewidth',2);
        end
        hold off;

        
         axis([ Sstartindex(1), Sstartindex(end), -1.1*deltamax, 1.1*deltamax ]);
         xlabel('s (m)','FontSize',14);
         ylabel('\delta','FontSize',14);
         set(findall(findobj(gcf),'Type','axes'),'FontName','Arial','FontWeight','Bold','LineWidth',2,'FontSize',14);
         set(findall(findobj(gcf),'Type','text'),'FontName','Arial','FontWeight','Bold','FontSize',14);
         grid on; box on;
        
        if flagtrack
            title('Energy distribution when lost')
        else
            title('Energy distribution when scattered')
        end
        
    end

