TouschekLossMap is the main function to compute the losses over a segment of lattice, calling TouschekPerElement for each thick element. The way I used to work on the cluster was to divide the lattice in groups of elements and send as many runs of TouschekLossMap as possible in parallel.

I have also put various tools I wrote at the time to visualize the results and distributions of particles.

I apologize in advance for the lack of comments and my writing in matlab!