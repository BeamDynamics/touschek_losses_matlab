function [plottraj, coorxlast, coorylast, coord] = PlotTrajHist(opticswithap, histlossel, newlosscoorx, newlosscoory, newlosscoord, varargin)
%% Plots the position of lost particles at gieven element: varargin may contain a name of element of optics 
% and you can select one or more occurence of the element in the optics.
% PlotTrajHist(LatticeSmallerDriftswithap, histlosselx, newlosscoorx, newlosscoory, newlosscoord, 'ScraperSpacex', 11, 1);
%  histlossel, newlosscoorx, newlosscoory, newlosscoord as computed by traj.max
%  



if isempty(varargin)
    thickel = find(cellfun(@(el) el.Length, opticswithap) > 1e-9);    
    disp(['No request for position, plotting losses in ' opticswithap{thickel(1)}.FamName]);
    varargin = {opticswithap{thickel(1)}.FamName};
end

indel = cellfun(@(elname) findcells( opticswithap, 'FamName', elname ), varargin(1), 'un', 0 );


if length(varargin)>1 && isnumeric(varargin{2})
    indel={indel{:}(varargin{2})};
end
if length(varargin)>2
    flagplot=varargin{3};
else
    flagplot=1;
end


eloflost = cellfun(@(elkind) cell2mat(...
    arrayfun(@(elnumbofkind) find( cell2mat(...
    cellfun(@(el) el(end)==elnumbofkind, histlossel, 'un', 0)) ),...
    elkind, 'un', 0)'),...
    indel, 'un', 0);


if ~isempty(eloflost{:}) 
    coorxlast  = cellfun(@(elkind) cell2mat(arrayfun(@(elnumofkind) newlosscoorx{elnumofkind}(:,end), elkind, 'un', 0)), eloflost, 'un', 0);
    coorxfirst = cellfun(@(elkind) cell2mat(arrayfun(@(elnumofkind) newlosscoorx{elnumofkind}(:,1),   elkind, 'un', 0)), eloflost, 'un', 0);
    coorylast  = cellfun(@(elkind) cell2mat(arrayfun(@(elnumofkind) newlosscoory{elnumofkind}(:,end), elkind, 'un', 0)), eloflost, 'un', 0);
    cooryfirst = cellfun(@(elkind) cell2mat(arrayfun(@(elnumofkind) newlosscoory{elnumofkind}(:,1),   elkind, 'un', 0)), eloflost, 'un', 0);
    
    coord = cellfun(@(elkind) cell2mat(arrayfun(@(elnumofkind) newlosscoord{elnumofkind}(:,end), elkind, 'un', 0)), eloflost, 'un', 0);
    deltamax=max( cell2mat( cellfun(@(elkind) max(elkind), coord, 'un', 0) ) );
    
    if flagplot==1
    
        plottraj = figure('Name','Position history','units','normalized','position',[0.35 0. 0.5 0.8]);hold on
        set(gcf,'Color',[1. 1. 1.]);
        
        perc=0.5;
        
        for i=1:length(coorxlast)
            subplot(length(coorxlast),1,i)
            plot( coorxfirst{i}(1:perc*length(coorxfirst{i})), cooryfirst{i}(1:perc*length(coorxfirst{i})), 'black x');
            hold on;
            scatter(coorxlast{i}(1:perc*length(coorxfirst{i})), coorylast{i}(1:perc*length(coorxfirst{i})), 15, coord{i}(1:perc*length(coorxfirst{i})), 'fill');
            %plot( coorxlast{i}, coorylast{i}, 'r x');
            hold off;
            
            view(0,90);
            c=colorbar(gca) ;
            %caxis([-deltamax, deltamax]);
            caxis([-0.2, 0.2]);
            
            xlabel('x (m)','FontSize',14);
            xlim([-0.05 0.05])
            ylabel('y (m)','FontSize',14);
            ylim([-0.02 0.02])
            box on; grid on;
            dist = findspos(opticswithap, indel{i}(1))-findspos(opticswithap, mod( indel{i}(1)-10, length(opticswithap)) );
            l = legend([ num2str(dist) ' m before the loss' ], ['Entrance of ' varargin{i}] );
            %t = title(varargin{i});
            set(l, 'interpreter', 'none')
            %set(t, 'interpreter', 'none')
        end
        ylabel(c,'\delta')
        set(findall(findobj(gcf),'Type','axes'),'FontName','Arial','FontWeight','Bold','LineWidth',2,'FontSize',14);
        set(findall(findobj(gcf),'Type','text'),'FontName','Arial','FontWeight','Bold','FontSize',14);
    else 
        plottraj=[];
    end
else
    disp('No lost particle.')
    plottraj=[]; coorxlast=[]; coorylast=[]; coord=[];
end
end



%     slist = findspos( opticswithap, histlossel{eloflost})'- findspos(opticswithap, listel(i) )  ;
%     subplot(4,1,1)
%     plot(repmat( slist, 1, size(newlosscoorx{eloflost}', 2)), newlosscoorx{eloflost}');
%     ylim([-0.1 0.1])
%     hold on;
%     subplot(4,1,2)   
%     plot(repmat( slist, 1, size(newlosscoorx{eloflost}', 2)), newlosscoory{eloflost}');
%     ylim([-0.1 0.1])
%     hold on;
%     subplot(4,1,4)  
%      hold on;
%     plot( sqrt(newlosscoorx{eloflost}(:,end).^2+newlosscoory{eloflost}(:,end).^2)-...
%           sqrt(newlosscoorx{eloflost}(:,1).^2+newlosscoory{eloflost}(:,1).^2), 'r x');
%     ylim([-0.1 0.1])
