function [ losslocations, lossinfo ] = BremLossMap( opticswithap,...
    bremsumgasdensity, negmomaccvselement, startindex, Nturns, Npart, first, last, varargin )
%% Generates the losses from bremstrahlung on residual gas around the ring.
%   opticswithap is the lattice structure, with physical aperture elements,
%   sumgasdenity is the table of double sum Sum(atoms)Sum(gas) Zj^2 alphaij
%   pi/kT at each element (ResidualGasElbyEl.m),
%   negmomaccvselement is the negative momentum acceptance from which particles are considered to
%   be lost if they interact at element #startindex (thick elements, length(momacc) < length(opticswithap)) ,
%   Nturns is the number of turns lost particles will be tracked to record
%   the loss
%   Npart is the number of particles generated at each element
%   first and last give the element numbers where to compute the losses
%   varargin may contain sigmas to impose a different value than the equilibrium calculated by atx, and
% epsy (one or the other can be 'auto').
%
%   copied from coulomb scattering and touschek function

tic;

r0 = 2.81794e-15;
c = 2.99792e8;
%gamma = atenergy(opticswithap) / 511.e3;
%beta = sqrt(1 - 1 / gamma^2);
cstfactor = (4*r0^2*c) / (137);


% Losses around the ring from all elements
lengthlist = cellfun(@(el) el.Length, opticswithap);
maskthick = lengthlist > 1e-9;
thickelem = find(maskthick);

listelem = thickelem( ismember( thickelem, (first:last) ) ); %% 

losslocations = cell(length(listelem),1);
lossinfo = cell(length(listelem),1);

    warning('off', 'all');
    if isempty(findcells(opticswithap,'PassMethod', '\w*Rad\w*'))
        [~, param] = atx(opticswithap, 0, 1:length(opticswithap));
    else
        [~, param] = atx(opticswithap, 0, 1:length(opticswithap), @noradon);        
    end
%    [beamdata, param] = atx(opticswithap);
%    param.modemittance
    sigmas = param.blength; 
    if isempty(varargin), 
        emity = param.modemittance(2);
    else
        if strcmp(varargin{1}, 'auto'), sigmasratio = 1; 
        else sigmasratio = varargin{1}/param.blength;
        end
        if strcmp(varargin{2}, 'auto'), emity = param.modemittance(2); else emity = varargin{2};end
    end
        
    warning('on', 'all');
    [M66, Ts, co]    = findm66(atradoff(opticswithap,''), listelem);
    a66=amat(M66);
    [G1, G2, G3] = find_inv_G_fromA(a66);
    S66 = jmat(3) ;
    beam66_s0  = -S66 * ( param.modemittance(1)*G1 + emity*G2 + param.modemittance(3)*G3 ) * S66;

%     beam66_s0(6,:) = sigmasratio * beam66_s0(6,:);   %  wrong, modified on 09/06/2015 according to SigmaMatrix6D.nb

    beam66_s0(6,6) = beam66_s0(6,6) - param.blength^2 + sigmas^2;
    beam66_s0 = triu(beam66_s0) + triu(beam66_s0,1)';


    listbeam66 = arrayfun( @(el) Ts(:,:,el) * beam66_s0 * Ts(:,:,el).', 1:length(listelem), 'Uniformoutput', false );
%    listbeam66 = arrayfun( @(el) eye(6) * beam66_s0 * eye(6).', 1:length(listelem), 'Uniformoutput', false );


negmomaccvselement = abs(negmomaccvselement);
elbyeltauinv = cstfactor .*(4/3*log(1./negmomaccvselement)-5/6) .* bremsumgasdensity(maskthick);

for counter = 1:length(listelem)
    el = listelem(counter);
    gasdata = bremsumgasdensity(el);
    negmomacc = negmomaccvselement(startindex==el);
    tauinvel = elbyeltauinv(startindex==el);
    [losslocations{counter}, lossinfo{counter}] = BremScatteringPerElement( opticswithap, el,...
                                                                            gasdata, co(:, counter), listbeam66{counter},...
                                                                            negmomacc, tauinvel, Nturns, Npart );
end


toc

end

