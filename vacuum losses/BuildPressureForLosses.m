function pressure = BuildPressureForLosses(Pprofile,sring,Pprofileinj,columns)
%% interpolate pressure data at the s values given by sring in m.
% Pprofileinj,columns are optional arguments allowing to modify the profile
% for the first and last cells (injection section). 
% columns is the columns of Pprofile to be modifyed.

        % adjust s values to fit the length period to avoid the profile to shift
        % when repeating it
        if Pprofile(1,1)==0
            Pprofile(1,:)=[];
        end
        
        sval   = [Pprofile(:,1)./1000; sring(end)/32]; % missing s=lcell
        [nsval, onaxisvalueidx] = unique(sval);
        ls1cell = length(nsval);

        lcell = nsval(end);
        shifts= lcell*cell2mat(arrayfun(@(i)i*ones(length(nsval),1)', 0:31, 'un', 0));
        nsval = [0; (repmat(nsval(1:end),32,1)+shifts')];
         
        % repeat p values
        npval = [Pprofile(:,2:end).*100 ; Pprofile(end,2:end).*100];  % missing last step, *100 to have pressure in Pa
        npval = [npval(1,:); repmat(npval,32,1)]; % s=0, then repeat 32 times
        
        if nargin>2 
            npvalinj = Pprofileinj(:,2:end).*100 ;  % missing last step, *100 to have pressure in Pa
            newpvalinj = [interp1(Pprofileinj(:,1)./1000,npvalinj,nsval(1:ls1cell+1),'linear','extrap');...
                        interp1(Pprofileinj(:,1)./1000,npvalinj,nsval(1:ls1cell+1)+nsval(ls1cell+1),'linear','extrap')];
            npval((end-ls1cell):end,[1 ,columns])=newpvalinj(1:ls1cell+1,:);
            npval(1:ls1cell+1,[1 columns]) = newpvalinj(ls1cell+2:end,:);
        end
        
        % interpolate at the lattice points
        pressure = interp1(nsval,npval,sring,'linear','extrap');
        
        
end