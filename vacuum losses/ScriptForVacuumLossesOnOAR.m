
%% Vacuum Losses on OAR
% R. Versteegen - 02/10/2014

% Script file to calculate vacuum losses of a lattice with physical aperture
% (called LatticeSmallerDriftswithap and aplimits).

% maj 31/03/2017 to provide PB with vacuum losses distributions

disp(' ****** Residual Gas Interactions Script ****** ')


% %% Compilation
% cd('/mntdirect/_users/versteeg/ESRFUpgrade/vacuumlosses/On_OAR/Functions/')
% mcc('-vm', 'OAR_LossMap', 'passmethodlistfringes'); % compile the function
% mcc('-vm', 'OAR_BremLossMap', 'passmethodlistfringes'); % compile the function
% exit; % exit matlab to release the licences

rootf = '/mntdirect/_users/versteeg/ESRF/vacuumlosses/';
rootfl = '/mntdirect/_users/versteeg/ESRF/VacuumConditioning/S28D/';

%% Get the lattice and Acceptances
% run('/mntdirect/_users/versteeg/ESRF/touscheklosses/FunctionCalls_2014Lattice.m')
% run('/mntdirect/_users/versteeg/ESRF/touscheklosses/ScriptForLatticeDescriptionS28D.m')
condcases = [1 3 5];
condcases = [1];
for nseed = 1:1;
    for condcase = condcases
openscrapers=1;
injdetail = 1;

%% vertical angle
% openscrapers=0;
% for nseed=1:10
%     if openscrapers
%         opencolli = 'opencolli';
%     else
%         opencolli = 'closedcolli';
%     end
%     load(['/mntdirect/_users/versteeg/ESRF/S28D-latticeswithcollimators/LatticeSeed' num2str(nseed) opencolli '.mat'],'LatticeSmallerDriftswithap');
% 
%     plotfolderscr = [rootf '/Results/Seed' num2str(nseed) opencolli '/'];
%     mkdir(plotfolderscr)
%     cd(plotfolderscr)
%     
%     save(['LatticeSeed' num2str(nseed) opencolli '.mat'],'LatticeSmallerDriftswithap');
%     run('/mntdirect/_users/versteeg/ESRF/vacuumlosses/ScriptForYAcceptanceOnOAR_esrf2.m')
% end
if openscrapers
    opencolli = 'opencolli';
else
    opencolli = 'closedcolli';
end
plotfolderscr = [rootf '/Results/Seed' num2str(nseed) opencolli '/'];
load([plotfolderscr 'LatticeSeed' num2str(nseed) opencolli '.mat'],'LatticeSmallerDriftswithap');
load([plotfolderscr 'angacc-pl4-6part-50turns-2it-RF1-Rad1opencolli.mat'],...
    'Lattice_startindex', 'Lattice_AngleAcc')

    EAp = atgetcells(LatticeSmallerDriftswithap,'EApertures');
    invacs = find(cell2mat(arrayfun(@(i)LatticeSmallerDriftswithap{i}.EApertures(2),find(invacs),'un',0))==0.003)
      
    LatticeSmallerDriftswithap = atsetfieldvalues(LatticeSmallerDriftswithap, [lastq2end(maskap') 1:firstq-1],'RApertures', ApSeptum );


aplimits  = atapinterpol( LatticeSmallerDriftswithap );



%% momentum
% openscrapers=1;
% for nseed=1:10
%     if openscrapers
%         opencolli = 'opencolli';
%     else
%         opencolli = 'closedcolli';
%     end
%     load(['/mntdirect/_users/versteeg/ESRF/S28D-latticeswithcollimators/LatticeSeed' num2str(nseed) opencolli '.mat'],'LatticeSmallerDriftswithap');
% 
%     plotfolderscr = [rootf '/Results/Seed' num2str(nseed) opencolli '/'];
%     mkdir(plotfolderscr)
%     cd(plotfolderscr)
%     
%     save(['LatticeSeed' num2str(nseed) opencolli '.mat'],'LatticeSmallerDriftswithap');
%     run('/mntdirect/_users/versteeg/ESRF/touscheklosses/ScriptForMomentumAcceptanceOnOAR.m')
% end
load([['/mntdirect/_users/versteeg/ESRF/touscheklosses/',...
    '/Results/Seed' num2str(nseed) opencolli '/' 'momacc-6part-1000turns-2it-RF1-Rad1.mat']])

%% Parameters


%TotP = 100*1.4e-9; % P in mbar *100=P in Pa
TotP = 100*1e-9; % P in mbar *100=P in Pa
% GasComp = { { {0.9 [1 2]}; {0.1 [7 2]} } };  % test
% GasComp = { { {0.97 [1 2]}; {0.01 [6 1; 1 4]}; {0.01 [6 1; 8 1]}; {0.01 [6 1; 8 2]} } };  % no current
 GasComp = { { {0.89 [1 2]}; {0.01 [6 1; 1 4]}; {0.09 [6 1; 8 1]}; {0.01 [6 1; 8 2]} } };  % 200mA (7/8mode)

detP=1;
% condcasestr = {'3000A.h','1000A.h','100A.h','10A.h','1A.h'};
% condcase=3;
% Pprofile = xlsread('Rev5_Assembly_200mA_3000Ah.xlsx',4);
% save([rootfl 'Pressure1cell-1-10-100-1000-3000Ah.mat'],'Pprofile' )
% load([rootfl 'Pressure1cell-1-10-100-1000-3000Ah.mat'],'Pprofile');
% ---- new version with mass 28 - HPM oct 2016:
if detP
    load([rootfl 'Pressure1cell2-1-10-50-100-500-1000Ah'],'Pprofile');
    load([rootfl 'PressureInj-1-50-500Ah.mat'],'Pprofileinj' );
else
end
condcasestr = {'1A.h','10A.h','50A.h','100A.h','500A.h','1000A.h'};
%condcase=4;
TotP = mean(Pprofile(:,condcase+1)*100)

pressurebump=0;
LeakP = 100*700e-9; % P in mbar *100=P in Pa
LeakComp={{ {0.2 [8 2]}; {0.8 [7 2]} }};

plane = 'Y'; pl=4;

 NturnsCS = 50; NturnsBR = 60;
 if openscrapers==1; NturnsBR = 1000; end% number of particles to be generated at each thick elements, max number of turns the lost ones will be tracked
 NpartV=1e6; sigs = 15.3e-12*2.99792e8; emity = 5e-12; % 25042017 Z=0.67
 NpartT=1e6;
% Npart = 1.3e7;sigs = 48e-12*2.99792e8; emity = 10e-12;

rfon         = 1;     
Voltage      = 6.5e6; 
HarmNum      = 992;  %  To include the longitudinal motion, energy gain in the cavities is 0.
radon        = 1;    %  To also include the energy loss per turn, compensated by an energy gain in the cavities.

coresavailable = 500;


%% Loading files and Setting folders



idxcav = findcells( LatticeSmallerDriftswithap, 'Class', 'RFCavity' );
if rfon && ~radon
    LatticeSmallerDriftswithap = atsetfieldvalues( LatticeSmallerDriftswithap, idxcav, 'PassMethod', 'CavityPass' );
    LatticeSmallerDriftswithap = atsetcavity( LatticeSmallerDriftswithap, Voltage, 0, HarmNum );
elseif rfon && radon, 
    LatticeSmallerDriftswithap = atradon( LatticeSmallerDriftswithap ); % changes the passmethod to cavitypass
    LatticeSmallerDriftswithap = atsetcavity( LatticeSmallerDriftswithap, Voltage, 1, HarmNum );
elseif ~rfon
    LatticeSmallerDriftswithap = atsetfieldvalues( LatticeSmallerDriftswithap, idxcav, 'PassMethod', 'DriftPass' );
    if radon, disp('Radiation cannot be ON without RF.'); end
end;
% 

 
%% Build the residual gas description
% repeating one cell profile 32 times (approx, does not deal with unequal straight sections)
sring = findspos(LatticeSmallerDriftswithap, 1:length(LatticeSmallerDriftswithap));

if detP   
    if injdetail
       intpval = BuildPressureForLosses(Pprofile,sring,Pprofileinj,condcases+1);
    else
        intpval = BuildPressureForLosses(Pprofile,sring);
    end
    if any(isnan(intpval)); disp('stop due to nan in pressure table.');return; end
    pressure = intpval(:,condcase);
    precname = ['-detP-' condcasestr{condcase}];
else
    pressure = TotP;
    precname = '';
end
%     figure;plot(findspos(LatticeSmallerDriftswithap,1:length(LatticeSmallerDriftswithap)),pressure,'b-x')
% return
if pressurebump && length(pressure)==length(LatticeSmallerDriftswithap)

    idxinterval = 840:885; % approx between s=50 and 53m
    
    pressure(idxinterval) = LeakP; 
    figure;plot(findspos(LatticeSmallerDriftswithap,1:length(LatticeSmallerDriftswithap)),pressure,'b-x')
    atplotsyn(gca,LatticeSmallerDriftswithap)
    
    GasComp=repmat(GasComp, length(LatticeSmallerDriftswithap), 1);
    GasComp(idxinterval) = repmat(LeakComp,length(idxinterval),1);
    
    precname = [precname '-pbump'];
end

CSsumdensity   = ResidualGasElbyEl( length(LatticeSmallerDriftswithap), pressure, GasComp, 'c' );
BRemsumdensity = ResidualGasElbyEl( length(LatticeSmallerDriftswithap), pressure, GasComp, 'b' );

%% Average lifetime


[tot,cs,bs] = atFullVacuumLifetime(LatticeSmallerDriftswithap, aplimits, pressure, GasComp, 'Y', ESRFUpgrade_MomAcc(:,1) );
 tau_CS     = 1/cs/3600
 tau_B      = 1/bs/3600
 tau_totvac = 1/(cs+bs)/3600




%% Loading files and Setting folders

disp('Loading structure and setting up parameters for vacuum loss map...');

foldername = ['S28D-S' num2str(nseed) '-' num2str(opencolli) num2str(condcase) '/']; % folder name in OAR folder data

mkdir([rootf 'On_OAR/Functions/' foldername])
mkdir([rootf 'On_OAR/Input/'     foldername])
mkdir([rootf 'On_OAR/Output/'    foldername])


% path for OAR
param = [rootf 'On_OAR/Functions/' foldername]; mkdir(param)
in    = [rootf 'On_OAR/Input/'     foldername]; mkdir(in) 
out   = [rootf 'On_OAR/Output/'    foldername]; mkdir(out)
PathToLossmapfunction     = [rootf 'On_OAR/Functions/run_OAR_LossMap.sh'];
PathToBremLossmapfunction = [rootf 'On_OAR/Functions/run_OAR_BremLossMap.sh'];

step = floor(length(LatticeSmallerDriftswithap)/coresavailable)+1; 
jobperproc = DivideOARJobs(length(LatticeSmallerDriftswithap),coresavailable);

complname = [num2str(step) 'el' num2str(NpartV) 'part' num2str(NturnsCS) 'tCS-' num2str(NturnsBR) 'tBR' precname]
paramfile = [complname '-param.txt'];
infile = [in complname];
outfile = [out complname];





%% Running on OAR cluster
disp('Generating parameter files for OAR (CS simul)...')


Ncommands = WriteParamFileLossMap( LatticeSmallerDriftswithap, plane, NturnsCS, NpartV, CSsumdensity,...
    Lattice_AngleAcc, Lattice_startindex, [param  'CS-' paramfile], [infile '-CS'], [outfile '-CS'], step )

% then go to the param folder (where the out and err files will be saved) lauch the jobs as an array : 
cd(param)
disp(['Launching ' num2str(Ncommands) ' jobs on OAR cluster...'])
SystemCommandToRun = ['oarsub -n ' 'VLossMap-CS' ...
    ' -l walltime=7 -q asd --array-param-file ./' 'CS-' paramfile ' ' PathToLossmapfunction];
status = system(SystemCommandToRun);
if status ~= 0, disp('Command failed.'), end

%% Running on OAR cluster
disp('Generating parameter files for OAR (BRems. simul)...')


Ncommands = WriteParamFileBremLossMap( LatticeSmallerDriftswithap, BRemsumdensity, ESRFUpgrade_MomAcc(:,1), Lattice_startindex, NturnsBR, NpartV, sigs, emity,...
     [param 'BR-' paramfile ], [infile '-BR'], [outfile '-BR'], step )

 % then go to the param folder (where the out and err files will be saved) lauch the jobs as an array : 
cd(param)
disp(['Launching ' num2str(Ncommands) ' jobs on OAR cluster...'])
SystemCommandToRun = ['oarsub -n ' 'VLossMap-BR' ...
    ' -l walltime=7 -q asd --array-param-file ./' 'BR-' paramfile ' ' PathToBremLossmapfunction];
status = system(SystemCommandToRun);
if status ~= 0, disp('Command failed.'), end

cd(rootf)

%% Wait to extract CS results

disp('Waiting for CS and BR loss map jobs to finish...')

cd([rootf 'On_OAR/Input/' foldername])
a=dir([complname '-CS' '*.mat']);
totproc=length(a);

cd([rootf 'On_OAR/Output/' foldername])
b=dir([complname '-CS' '*.mat']);
finproc=length(b);
timeit=0;
% wait for processes to finish
while finproc~=totproc
    % wait
    pause(60);
     timeit=timeit+1;
    % count runnning processes
    b=dir([complname '-CS' '*.mat']);
    finproc=length(b);  
    disp([num2str(finproc) '/' num2str(totproc) ' CS loss map jobs are finished, running since ' num2str(timeit) 'min'])
end

cd(rootf)

%% Wait to extract BR results

disp('Waiting for BR loss map jobs to finish...')

cd([rootf 'On_OAR/Input/' foldername])
a=dir([complname '-BR' '*.mat']);
totproc=length(a);

cd([rootf 'On_OAR/Output/' foldername])
b=dir([complname '-BR' '*.mat']);
finproc=length(b);
timeit=0;
% wait for processes to finish
while finproc~=totproc
    % wait
    pause(60);
     timeit=timeit+1;
    % count runnning processes
    b=dir([complname '-BR' '*.mat']);
    finproc=length(b);  
    disp([num2str(finproc) '/' num2str(totproc) ' BR loss map jobs are finished, running since ' num2str(timeit) 'min'])
end

cd(rootf)


%% Extract results

disp('Extracting CS results...')

clear CSlosslocations CSlossinformations;
n=1;
CSlosslocations  = [];  
CSlossinformations = [];

for counter = 1:step:length(LatticeSmallerDriftswithap)
    try
        load( [outfile '-CS_' num2str(counter) '.mat'] );
        CSlosslocations =  [CSlosslocations losslocations'];
        CSlossinformations =  [CSlossinformations lossinfo'];
        n=n+1;
    catch
        disp([[outfile '-CS_' num2str(counter) '.mat'] ' not found.'])
    end
end
disp('... done.')


%% Extract results

disp('Extracting BR results...')

clear BRlosslocations BRlossinformations;
n=1;

BRlosslocations  = [];  
BRlossinformations = [];
for counter = 1:step:length(LatticeSmallerDriftswithap)
    try
        load( [outfile '-BR_' num2str(counter) '.mat'] );
        BRlosslocations =  [BRlosslocations losslocations'];
        BRlossinformations =  [BRlossinformations lossinfo'];
        n=n+1;
    catch
        disp([[outfile '-BR_' num2str(counter) '.mat'] ' not found.'])
    end
end

disp('... done.')

% cd([rootf 'On_OAR/Functions/' foldername])
% delete('*')
% cd([rootf 'On_OAR/Output/' foldername])
% delete('*')
% cd([rootf 'On_OAR/Input/' foldername])
% delete('*')
% 
% return

%% Save

save([plotfolderscr '/CSLosses-' complname 'injdetails' num2str(injdetail) '.mat'], 'CSlosslocations', 'CSlossinformations');
disp([ 'Results saved as ' [plotfolderscr '/CSLosses-' complname 'injdetails' num2str(injdetail) '.mat'] ])
save([plotfolderscr '/BRLosses-' complname 'injdetails' num2str(injdetail) '.mat'], 'BRlosslocations', 'BRlossinformations');
disp([ 'Results saved as ' [plotfolderscr '/BRLosses-' complname 'injdetails' num2str(injdetail) '.mat'] ])




%% Delete OAR files

cd([rootf 'On_OAR/Functions/' foldername])
delete('*')
cd([rootf 'On_OAR/Output/' foldername])
delete('*')
cd([rootf 'On_OAR/Input/' foldername])
delete('*')

    end
end
return

%% Plots and lifetime

rootf = '/mntdirect/_users/versteeg/ESRF/vacuumlosses/';
rootfl = '/mntdirect/_users/versteeg/ESRF/VacuumConditioning/S28D/';

condcasestr = {'1A.h','10A.h','50A.h','100A.h','500A.h','1000A.h'};
injdetail = 1;

avlt = [];
for openscrapers=[0 1]
tautable=[];
for nseed = 1:10;
    nseed
    for condcase = [1 3 5]
        condcase
        openscrapers;
        if openscrapers
            opencolli = 'opencolli';
        else
            opencolli = 'closedcolli';
        end
        
        plotfolderscr = [rootf '/Results/Seed' num2str(nseed) opencolli '/'];
        precname = ['-detP-' condcasestr{condcase}];
        NturnsBR = 60; if openscrapers==1; NturnsBR = 1000; end
        load([plotfolderscr '/LatticeSeed' num2str(nseed) opencolli '.mat'], 'LatticeSmallerDriftswithap');
        step = floor(length(LatticeSmallerDriftswithap)/coresavailable)+1; 
        complname = [num2str(step) 'el' num2str(NpartV) 'part' num2str(NturnsCS) 'tCS-' num2str(NturnsBR) 'tBR' precname];
        load([plotfolderscr '/CSLosses-' complname 'injdetails' num2str(injdetail) '.mat'], 'CSlosslocations', 'CSlossinformations');
        load([plotfolderscr '/BRLosses-' complname 'injdetails' num2str(injdetail) '.mat'], 'BRlosslocations', 'BRlossinformations');

%         figu=CombinedLossMapPlot( LatticeSmallerDriftswithap,...
%             {CSlosslocations,BRlosslocations}, [NpartV,NpartV], {'e^--A, elastic', 'e^--A, inelastic'}, -1, {'B1H', 'B2S'} );
%        saveas(figu, [plotfolderscr '/LossMapBetweenDipoles.fig'], 'fig'); close(figu);
        [figu,~,~,tauinv]=CombinedLossMapPlot( LatticeSmallerDriftswithap,...
            {CSlosslocations,BRlosslocations}, [NpartV,NpartV], {'e^--A, elastic', 'e^--A, inelastic'}, -2, 32 );
        saveas(figu, [plotfolderscr '/SuperImposedLosses' precname '.fig'], 'fig'); close(figu);
        figu=TurnHistPlot( CSlossinformations, NturnsCS )
        saveas(figu, [plotfolderscr '/TurnsCS' precname '.fig'], 'fig'); close(figu);
        figu=TurnHistPlot( BRlossinformations, NturnsBR )
        saveas(figu, [plotfolderscr '/TurnsBR' precname '.fig'], 'fig'); close(figu);
        tautable = [tautable;[nseed tauinv]];
    end
end

for condcase=1:3
    lt = [mean(tautable(condcase:3:end,2:3)) std(tautable(condcase:3:end,2:3)) ];
    avlt = [avlt; [openscrapers condcase 1./lt(1:2)./3600 1/(lt(1)+lt(2))/3600]];
end

end
lttosave={'openscrapers conditioning case (1 50 500)A.h ltCS ltBR meanlt',avlt};
save(['/mntdirect/_users/versteeg/ESRF/vacuumlosses//Results/Lifetime.mat'],'lttosave')

%% One file per cell per seed for PB - 27042017
disp('Generating .txt files with lost particles coordinates...')

rootf = '/mntdirect/_users/versteeg/ESRF/vacuumlosses/';
rootfl = '/mntdirect/_users/versteeg/ESRF/VacuumConditioning/S28D/';

condcasestr = {'1A.h','10A.h','50A.h','100A.h','500A.h','1000A.h'};
injdetail = 1;

for openscrapers=[0 1]
    for nseed = 1:10;
        nseed
        for condcase = [1 3 5]
            condcasestr{condcase}
            openscrapers;
            if openscrapers
                opencolli = 'opencolli';
            else
                opencolli = 'closedcolli';
            end
            
            plotfolderscr = [rootf '/Results/Seed' num2str(nseed) opencolli '/'];
            precname = ['-detP-' condcasestr{condcase}];
            NturnsBR = 60; if openscrapers==1; NturnsBR = 1000; end
            complname = [num2str(step) 'el' num2str(NpartV) 'part' num2str(NturnsCS) 'tCS-' num2str(NturnsBR) 'tBR' precname];
            load([plotfolderscr '/CSLosses-' complname 'injdetails' num2str(injdetail) '.mat'], 'CSlosslocations', 'CSlossinformations');
            load([plotfolderscr '/BRLosses-' complname 'injdetails' num2str(injdetail) '.mat'], 'BRlosslocations', 'BRlossinformations');
            load([plotfolderscr '/LatticeSeed' num2str(nseed) opencolli '.mat'], 'LatticeSmallerDriftswithap');
            
            emptylossel = cell2mat(arrayfun(@(i)size(i{:},1),CSlossinformations,'un',0)');
            CSlossinformations(~emptylossel) = {struct('lost', [], 'turn', [],'element', [], 'coordinates', [],'scatteringangle',[])};
            emptylossel = cell2mat(arrayfun(@(i)size(i{:},1),BRlossinformations,'un',0)');
            BRlossinformations(~emptylossel) = {struct('lost', [], 'turn', [],'element', [], 'coordinates', [],'ddelta',[])};
            
            filefolder = [rootf '/Results/S28D-CoordinatesFiles/' opencolli '/S28D-S' num2str(nseed) '/' ];
            mkdir(filefolder);
            filename = [ precname(7:end-3) 'Ah-LostParticlesCS'];
            GenerateCoordinatesFilePerCell(LatticeSmallerDriftswithap, cell2mat(CSlossinformations), 'ID\w*', [ filefolder filename ]);
            filename = [ precname(7:end-3) 'Ah-LostParticlesBR'];
            GenerateCoordinatesFilePerCell(LatticeSmallerDriftswithap, cell2mat(BRlossinformations), 'ID\w*', [ filefolder filename ]);
        end
    end
end
