function [ LossLocations, LossInfoElement ] = ScatteringPerElement( opticswithap,...
    indexelement, sumgasdensity, maxanglescattering, tauinvel, Nturns, Npart, plane )
%% ScatteringPerElement( opticswithap, indexelement, sumgasdensity, maxanglescattering, Nturns, Npart, plane )
%% Computes the losses from Coulomb scattering generated at a given element.
%
%  opticswithap is the lattice structure (with aperture elements to compute the losses),
%  indexelement refers to the element to track from, where the scattering
%  occurs,
%  sumgasdensity is the result of Sum(atoms)Sum(gas) Zj^2 alphaij pi/kT at the element
%  (as produced by the function ResidualGasElbyEl),
%  maxanglescattering is the angle limit from which particles scattered at
%  the element are lost,
%  Nturns is the number of turns required to simulate the losses
%  (tracking).
%  Npart is the number of particles to be generated.
%  plane is 'X' or 'Y'

lengthel = opticswithap{indexelement}.Length;
LossLocations = NaN;
LossInfoElement = [];

if lengthel == 0
    disp('The element index corresponds to a thin element, please choose the thick element next to it.')
else    
    r0 = 2.81794e-15;
    c = 2.99792e8;
    gamma = atenergy(opticswithap) / 511.e3;
    beta = sqrt(1 - 1 / gamma^2);
    cstfactor = (8*pi*r0^2*c) / (gamma^2*beta^3);
        
    LossProba = lengthel * tauinvel;
    npartlost = round( Npart * LossProba );
    
    thetascattering=[];n=0;generator=0;
    while n~= npartlost
        % random probability of interaction for each particle
        s = RandStream('mcg16807','Seed',indexelement*length(opticswithap)+generator); % to be sure the seed is not repeated while looping over the elements
        RandStream.setGlobalStream(s);
        proba = rand(2, 1);
              
        % multiply by the particle density to get the differential cross section
        diffcrosssection = proba(1) / sumgasdensity / lengthel;
        
        % deduce the scattering angle
        thetascatteringpart = (cstfactor./diffcrosssection).^(1/3);
        % random sign
        if proba(2)<0.5,  thetascatteringpart = -thetascatteringpart; end
        %hist(thetascattering, Npart) ;
        
        % keep only the angles bigger than thetamax
        if testlost(thetascatteringpart, maxanglescattering), n=n+1; thetascattering = [thetascattering;thetascatteringpart]; end;
        
        generator=generator+1;
    end

    if npartlost ~= 0
        % tracking of the remaining, neglecting initial coordinates
        if plane == 'X', vec = [0;1;0;0;0;0]; elseif plane == 'Y', vec = [0;0;0;1;0;0]; else vec=NaN; disp('Plane was not recognized.'), end
        
        rin = vec * thetascattering';
        opticstotrack = atrotatelattice( opticswithap, indexelement );
        
        [~, ~, ~, LossInfoElement] = ringpass(opticstotrack, rin, Nturns);
        LossInfoElement.scatteringangle = thetascattering;

        if isempty(LossInfoElement)
            lossinfopart = struct('lost', zeros(1,npartlost), 'turn', inf(1,npartlost),...
                'element', nan(1,npartlost), 'coordinates', nan(6,npartlost), 'scatteringangle', NaN);
            LossInfoElement = repmat(lossinfopart, npartlost, 1);
        end
        
        LossLocations = mod( indexelement-1 + LossInfoElement.element , length(opticswithap) );
        LossInfoElement.element = LossLocations;
        
    else
        disp(['Zero particle scattered at element #' num2str(indexelement) ' were lost.'])
    end
    
end

    function [lostpart] = testlost(theta, thetamax)
        if (theta<0 && theta <= thetamax(1)) || (theta>0 && theta >= thetamax(2))
            lostpart = true;
        else
            lostpart = false;
        end
    end

    function [mat] = z2(mat)
        mat(:,1) = mat(:,1).^2;
    end

end

