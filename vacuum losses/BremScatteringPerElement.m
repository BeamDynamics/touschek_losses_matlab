function [ LossLocations, LossInfoElement ] = BremScatteringPerElement( opticswithap,...
    indexelement, bremsumgasdensity, co, beamsigma, negmomacc, tauinvel, Nturns, Npart )
%% BremScatteringPerElement( opticswithap, indexelement, sumgasdensity, negmomacc, Nturns, Npart )
%% Computes the losses from Bremstrahlung interaction at a given element.
%
%  opticswithap is the lattice structure (with aperture elements to compute the losses),
%  indexelement refers to the element to track from, where the scattering
%  occurs,
%  sumgasdensity is the result of Sum(atoms)Sum(gas) Zj^2 alphaij pi/kT at the element
%  (as produced by the function ResidualGasElbyEl),
%  momacc is the angle limit from which particles scattered at
%  the element are lost,
%  Nturns is the number of turns required to simulate the losses
%  (tracking).
%  Npart is the number of particles to be generated.


lengthel = opticswithap{indexelement}.Length;
LossLocations = NaN;
LossInfoElement = [];

deltaEb=[];
generator=0;
n=0;

if lengthel == 0
    disp('The element index corresponds to a thin element, please choose the thick element next to it.')
else    
    r0 = 2.81794e-15;
    c = 2.99792e8;
    % E0 = atenergy(opticswithap);
    % gamma = E0 / 511.e3;
    % beta = sqrt(1 - 1 / gamma^2);
    cstfactor = (4*r0^2*c) / (137);
    negmomacc = abs(negmomacc);

    LossProba = lengthel * tauinvel;
    npartlost = round( Npart * LossProba );
    
    if npartlost >= 1
        
        probalim = cstfactor * bremsumgasdensity * 1/negmomacc * (4/3*(1-negmomacc) +negmomacc^2) * lengthel;
        
        partcoor = mvnrnd_r(co, beamsigma, npartlost)'; % random initial coordinates for each particle
                
        while n<npartlost
            
            s = RandStream('mcg16807','Seed',indexelement*length(opticswithap)+generator ); % to be sure the seed is not repeated while looping over the elements
            RandStream.setGlobalStream(s);
            proba = rand(npartlost-n, 1)*probalim; % random probability of interaction for each particle, smaller than the limit from acceptance
            
            % solve dsig = cstfactor*(1./Eb*(4/3*(1-Eb./E0)+(Eb/E0).^2))
            deltaEbit = -( 1/(6*cstfactor*lengthel*bremsumgasdensity) *...
                ( 3*proba+4*cstfactor*lengthel*bremsumgasdensity-...
                sqrt(9*proba.^2+24*cstfactor*lengthel*bremsumgasdensity*proba-32*cstfactor^2*lengthel^2*bremsumgasdensity^2) ) );
            
            deltaEbit(~isreal(deltaEbit))=[];
            deltaEbit(deltaEbit<-1)=[];
            deltaEb = [deltaEb; deltaEbit];
        
            generator = generator+1;
            n=length(deltaEb);
        end

        partcoor(5,:) = partcoor(5,:).*(1+deltaEb')+deltaEb';

        opticstotrack = atrotatelattice( opticswithap, indexelement );
        
        [~, ~, ~, LossInfoElement] = ringpass(opticstotrack, partcoor, Nturns);
        LossInfoElement.ddelta = partcoor(5,:)';
        
        if isempty(LossInfoElement)
            lossinfopart = struct('lost', zeros(1,npartlost), 'turn', inf(1,npartlost),...
                'element', nan(1,npartlost), 'coordinates', nan(6,npartlost), 'scatteringangle', NaN);
            LossInfoElement = repmat(lossinfopart, npartlost, 1);
        end
        
        LossLocations = mod( indexelement-1 + LossInfoElement.element , length(opticswithap) );
        LossInfoElement.element = LossLocations;
        
    else
        disp(['Zero particle scattered at element #' num2str(indexelement) ' were lost.'])
    end
        
end


end

