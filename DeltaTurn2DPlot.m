    function deltahist = DeltaTurn2DPlot( Sstartindex, lossesperel, NturnsMax, varargin )
        %% Number of turns performed by the scattered particles.
        %
        
        if nargin>3
            deltavsthickelement = varargin{1};
            if nargin>4
                momaccfactor = varargin{2};
            else
                momaccfactor = 1;
            end           
        else
            deltavsthickelement =[];   
        end

        lossturns  = arrayfun( @(el) el.turn,   lossesperel, 'UniformOutput', false);
        lossdeltas = arrayfun( @(el) el.ddelta, lossesperel, 'UniformOutput', false);

        if ~iscell(lossdeltas)
            lossturns(lossdeltas==0)=[];
            lossdeltas(lossdeltas==0)=[];
        end
        deltahist = figure('Name','2D - Turns','units','normalized','position',[0.35 0.4 0.5 0.5]); 
        set(gcf,'Color',[1. 1. 1.]);
        
        deltamax = max(abs(cell2mat(lossdeltas)));

        mat= arrayfun(@(el) [repmat(Sstartindex(el),length(lossdeltas{el}),1) lossdeltas{el}' lossturns{el}'],...
            1:length(Sstartindex), 'UniformOutput', false )';

        mat(cellfun(@(el2) ismember(0,el2), cellfun(@(el) size(el), mat, 'UniformOutput', false ) ))=[];
        mat=cell2mat(mat);

        scatter(mat(:,1), mat(:,2), 20, mat(:,3), 'fill'); 
        
        axis([ Sstartindex(1), Sstartindex(end), -1.1*deltamax, 1.1*deltamax ]);
        xlabel('s (m)','FontSize',14);
        ylabel('\delta','FontSize',14);

        %colormap('Pink');
        hold on;
        if ~isempty(deltavsthickelement)
        plot(Sstartindex, deltavsthickelement(:,1)/momaccfactor, 'black-',...
             Sstartindex, deltavsthickelement(:,2)/momaccfactor, 'black-','linewidth',2);
        line([Sstartindex(round(length(Sstartindex)/20)), Sstartindex(round(length(Sstartindex)/10))],[-deltamax -deltamax],'LineWidth',2,'Color','black') 
        text(Sstartindex(round(length(Sstartindex)/8)),-0.99*deltamax,'\it Calculated momentum acceptance (for x=0)')
        end
        hold off;

        title('Number of turns performed by the scattered particles')
        grid on; box on;

        view(0,90);
        c= colorbar(gca) ; %axes(c); 
        ylabel(c,'N_{turns}')
        caxis([0,NturnsMax])
     
        set(findall(findobj(gcf),'Type','axes'),'FontName','Arial','FontWeight','Bold','LineWidth',2,'FontSize',14);
        set(findall(findobj(gcf),'Type','text'),'FontName','Arial','FontWeight','Bold','FontSize',14);


        
    end

