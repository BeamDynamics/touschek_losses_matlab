    function [deltahist,deltascoordinates] = DeltaHistPlot( flagtrack, lossesperel, deltamax, Nbins,varargin )
        %% Delta distribution.
        % varargin allows to plot another coordinate after tracking 1-6
        
%         if flagtrack
%             listoflost = arrayfun( @(el) Lattice_lossinfo(el).lost, 1:length(Lattice_lossinfo), 'UniformOutput', false);
%             losses = Lattice_lossinfo(cellfun( @(el) ~isempty(find(el)), listoflost'));
%             lossesperel = losses(arrayfun(@(part)part.lost, losses));
%         end
        if ~isempty(varargin)
            coortoplot=varargin{1};
        else
            coortoplot=5;
        end
        labelx={'x', 'xp','y','yp','l','\delta'};
        labelx=labelx{coortoplot};
        
        lossturns = cell2mat(arrayfun( @(el) el.turn, lossesperel, 'UniformOutput', false));
        if ~flagtrack
            lossdeltas = cell2mat(arrayfun( @(el) el.ddelta, lossesperel, 'UniformOutput', false));
        else
            lossdeltas = cell2mat(arrayfun( @(el) el.coordinates(coortoplot,:,end), lossesperel, 'UniformOutput', false));
        end
        
        lossturns(lossdeltas==0)=[];
        lossdeltas(lossdeltas==0)=[];
                
        deltahist = figure('Name','Delta distribution','units','normalized','position',[0.35 0.4 0.5 0.5]); 
        set(gcf,'Color',[1. 1. 1.]);
        
        %deltamax = max( abs(lossdeltas) );
        [counts0,  bins] = hist( lossdeltas(isnan(lossturns)), linspace(-1.1*deltamax, 1.1*deltamax, Nbins) );
        [counts1,  bins] = hist( lossdeltas(lossturns==1), linspace(-1.1*deltamax, 1.1*deltamax, Nbins) );
        [countsN1L200, bins] = hist( intersect(lossdeltas(lossturns~=1),lossdeltas(lossturns<200)), linspace(-1.1*deltamax, 1.1*deltamax, Nbins) );
        [countsN1G200, bins] = hist( intersect(lossdeltas(lossturns~=1),lossdeltas(lossturns>200)), linspace(-1.1*deltamax, 1.1*deltamax, Nbins) );
        bar(bins,[counts0' counts1' countsN1L200' countsN1G200'], 'Stack','barwidth',1);
        
        axis([ -1.1*deltamax, 1.1*deltamax, 0, 1.1*max(counts0+counts1+countsN1L200+countsN1G200) ]);
        xlabel(labelx,'FontSize',14);
        ylabel('Counts','FontSize',14);
        legend('Not lost', 'Lost on 1st turn','Lost on turn 1< T < 200', 'Lost on turn T > 200',...
            'vertical plane','Location','NorthEast');
        set(findall(findobj(gcf),'Type','axes'),'FontName','Arial','FontWeight','Bold','LineWidth',2,'FontSize',14);
        set(findall(findobj(gcf),'Type','text'),'FontName','Arial','FontWeight','Bold','FontSize',14);
        grid on; box on;
        
        
        
        
    end

